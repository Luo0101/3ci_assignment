#assignment 
import numpy as np
import pandas as pd
from sklearn import linear_model
import matplotlib.pyplot as plt
import skfuzzy as fuzz
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_squared_error

data = np.array(pd.read_csv("Training_data.csv", skiprows = 0))
y = np.array(data[:,-1])
#part 1
print("Part 1:")
#remove outliner
y0 = y
Q1 = np.percentile(y0, 25)
Q3 = np.percentile(y0, 75)

range = [Q1-1.5*(Q3-Q1), Q3+1.5*(Q3-Q1)]
position = np.concatenate((np.where(y0>range[1]), 
                           np.where(y0<range[0])),axis = 1)

y_new = np.delete(y0, position)
X = np.arange(np.size(y_new)).reshape(-1,1)
# plot the p before delete
plt.plot(y0, 'o')
plt.show()

y0_new = np.delete(y0, position)

#plt.xlim(0, 1000)
plt.ylim(0, 500)

plt.plot(y0_new, 'o')
plt.show()


#part 2
#x = np.arange(0, len(y_new), 1).reshape(-1, 1)
print("Part 2:")
reg = linear_model.LinearRegression()
reg.fit(X, y_new)
plt.scatter(X, y_new, s=20, color='k', alpha=0.5)

x_min = X.min() - .5
x_max = X.max() + .5
xx = np.arange(x_min, x_max, 1)
xx = [[val] for val in xx]
yy = reg.predict(xx)
plt.plot(xx, yy, color='r', alpha=1)
# Show axis labels
plt.xlabel('Time')
plt.ylabel('P(t+1)')
plt.show()

#print("regression coefficient: ", reg.coef_)
#print("regression interception: ", reg.intercept_)

TargetOutput = yy
SystemOutput = y_new

RErr=np.sum(np.absolute(TargetOutput-SystemOutput)/
 np.absolute(TargetOutput))/len(TargetOutput)

print('The Average Relative Error Value is', RErr)
#print("average", np.sum(y_new/len(y_new)))

#part3
# create model
print("Part 3:")
reg = MLPRegressor(hidden_layer_sizes=(100,20), activation='logistic',
                     solver='adam', alpha=1e-4, max_iter=50000)

reg.fit(X, y_new)

y_pred = reg.predict(X)
print('The Mean Squared Error Value is',mean_squared_error(y_new, y_pred))

RErr=np.sum(np.absolute(y_new-y_pred)/
 np.absolute(y_new))/len(y_new)

print('The Average Relative Error Value is', RErr)

xx = X
yy = y_new
plt.scatter(X, y_new, s=20, color='k', alpha=0.5)

x_min = X.min() - .5
x_max = X.max() + .5
xx = np.arange(x_min, x_max, 1)
xx = [[val] for val in xx]
yy = reg.predict(xx)
plt.plot(xx, yy, color='r', alpha=1)
# Show axis labels
plt.xlabel('Time')
plt.ylabel('P(t+1)')
plt.show()

#part 4 
print("Part 4:")
X = np.array(data[:,:-1])
X_new = np.delete(X, position, 0)
A = np.row_stack((X_new[:,0], X_new[:,1], X_new[:,2], X_new[:,3], X_new[:,4], X_new[:,5],y_new)) 
CCM=np.corrcoef(A)
print(CCM)

t = np.arange(20,34,1)
d = np.arange(3500, 7000, 1)
p= np.arange(10, 55, 1)
t_cold = fuzz.trapmf(t, [20, 20, 20, 27])
t_warm = fuzz.trapmf(t, [22, 27, 27,31])
t_hot = fuzz.trapmf(t, [27, 34, 34, 34])

d_vl=fuzz.trapmf(d, [3500, 3500, 3500, 4250])
d_li=fuzz.trapmf(d, [3600, 4250, 4250, 5100])
d_mid=fuzz.trapmf(d, [4250 , 5250, 5250, 6250])
d_hi=fuzz.trapmf(d, [5250, 6000, 6000, 7000])
d_vh=fuzz.trapmf(d, [6250, 7000, 7000, 7000])

p_low = fuzz.trapmf(p, [10, 10, 10, 25])
p_norm = fuzz.trapmf(p, [10, 25, 25, 40])
p_high = fuzz.trapmf(p, [25, 55, 55, 55])

fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(8,9))
ax0.plot(t,t_cold, 'b', linewidth=1.5, label = 'Cold')
ax0.plot(t,t_warm, 'g', linewidth=1.5, label = 'Warm')
ax0.plot(t,t_hot, 'r', linewidth=1.5, label = 'Hot')
ax0.set_title('Input T')
ax0.legend()

ax1.plot(d,d_vl, 'b', linewidth=1.5, label = 'Very Little')
ax1.plot(d,d_li, 'g', linewidth=1.5, label = 'Little')
ax1.plot(d,d_mid, 'r', linewidth=1.5, label = 'Middle')
ax1.plot(d,d_hi, 'aqua', linewidth=1.5, label = 'High')
ax1.plot(d,d_vh, 'violet', linewidth=1.5, label = 'Very High')
ax1.set_title('Input D')
ax1.legend()

ax2.plot(p,p_low, 'b', linewidth=1.5, label = 'Low')
ax2.plot(p,p_norm, 'g', linewidth=1.5, label = 'Norm')
ax2.plot(p,p_high, 'r', linewidth=1.5, label = 'High')
ax2.set_title('Output P')
ax2.legend()

for ax in (ax0, ax1, ax2):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()    
plt.tight_layout()

t0= X_new[:,0]
d0 = X_new[:,5]

for t1,d1 in zip(t0,d0):
   pass
t_level_cold = fuzz.interp_membership(t, t_cold, t1)
t_level_warm = fuzz.interp_membership(t, t_warm, t1)
t_level_hot = fuzz.interp_membership(t, t_hot, t1)


d_level_vl = fuzz.interp_membership(d, d_vl, d1)
d_level_li = fuzz.interp_membership(d, d_li, d1)
d_level_mid = fuzz.interp_membership(d, d_mid, d1)
d_level_hi = fuzz.interp_membership(d, d_hi, d1)
d_level_vh = fuzz.interp_membership(d, d_vh, d1)


active_rule1 = np.fmin(t_level_cold, d_level_li)
p_activation_lo1 = np.fmin(active_rule1, p_low)

active_rule2 = np.fmin(t_level_hot, d_level_li)
p_activation_lo2 = np.fmin(active_rule2, p_low)

#active_rule3 = np.fmin(t_level_warm, d_level_vl)
#p_activation_lo3 = np.fmin(active_rule3, p_low)

active_rule3 = d_level_mid
p_activation_md1 = np.fmin(active_rule3, p_norm)

#active_rule4 = np.fmin(t_level_hot, d_level_mid)
#p_activation_md2 = np.fmin(active_rule4, p_norm)

#active_rule4 = np.fmin(t_level_warm, d_level_mid)
#p_activation_md3 = np.fmin(active_rule4, p_norm)

active_rule4 = np.fmin(t_level_cold, d_level_hi)
p_activation_hi1 = np.fmin(active_rule4, p_high)

active_rule5 = np.fmin(t_level_hot, d_level_vh)
p_activation_hi2 = np.fmin(active_rule5, p_high)

# Aggregate all three output membership functions together
#aggregated = np.fmax(p_activation_lo1, np.fmax(p_activation_lo2,np.fmax (p_activation_lo3 ,
#                    np.fmax (p_activation_md1, np.fmax(p_activation_md2,np.fmax( p_activation_md3,
#                    np.fmax(p_activation_hi1,p_activation_hi2)))))))
#print(len(aggregated)) 
#print(len(p))  
aggregated = np.fmax(p_activation_lo1, np.fmax(p_activation_lo2, np.fmax(p_activation_md1,
             np.fmax(p_activation_hi1, p_activation_hi2))))
# Calculate defuzzified result
price = fuzz.defuzz(p, aggregated, 'centroid') 

# Find the tip activation corresponding to tip
price_activation = fuzz.interp_membership(p, aggregated, price)

price = fuzz.defuzz(p, aggregated, 'centroid')
price_activation = fuzz.interp_membership(p, aggregated, price)

fig, ax3 = plt.subplots(figsize=(8, 3))
ax3.plot(p, p_low, 'b', linewidth=0.5, linestyle='--', )
ax3.plot(p, p_norm, 'g', linewidth=0.5, linestyle='--')
ax3.plot(p, p_high, 'r', linewidth=0.5, linestyle='--')
price0 = np.zeros_like(p)
ax3.fill_between(p, price0, aggregated, facecolor='Orange', alpha=0.7)
ax3.plot([price, price], [0, price_activation], 'k', linewidth=1.5, alpha=0.9)
ax3.set_title('Aggregated membership and result (line)')

# Turn off top/right axes
for ax in (ax3,):
 ax.spines['top'].set_visible(False)
 ax.spines['right'].set_visible(False)
 ax.get_xaxis().tick_bottom()
 ax.get_yaxis().tick_left()
plt.tight_layout()
